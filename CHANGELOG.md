# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.2](https://gitlab.com/elberthAgreda/testing/compare/v0.0.1...v0.0.2) (2022-12-05)

### 0.0.1 (2022-12-05)

## 1.0.0
- Esto es una prueba